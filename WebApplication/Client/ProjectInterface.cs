﻿using LINQ.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Task = LINQ.Model.Task;

namespace LINQ
{
    class ProjectInterface
    {

        string[] lINQmenu;
        string[] menu;
        string[] CRUD;
        string[] dataTypes;
        ProjectsService projectsService;
        TaskWorker taskWorker;

        public ProjectInterface(string APIUrl)
        {
            taskWorker = new TaskWorker();
            projectsService = new ProjectsService(APIUrl);
            menu = new string[]
            {
                "Work with projects",
                "Work with states",
                "Work with Tasks",
                "Work with Teams",
                "Work with Users",
                "Show Data",
                "Show all messages"
            };
            CRUD = new string[]
            {
                "Create",
                "Read",
                "Update",
                "Delete"
            };
            lINQmenu = new string[]
            {
                "Show projects and its tasksCount by UserId",
                "Show tasks with name shorter then 45 symbols by UserId",
                "Show (id,name) tasks, that was finished this year by UserId",
                "Show list of teams, all members of which are older than 12 years, sorted by registrationDate and grouped by teams",
                "Show list of users, sorted alphabetically with sorted tasks by name length",
                "Show structure, connected with user and his project, by its id",
                "Show structure, connected with Project and its tasks, by project id"
            };
            dataTypes = new string[]
            {
                "project",
                "state",
                "task",
                "team",
                "user"
            };
        }

        public void Start()
        {
            TypeInfo("To hide current menu type \"0\"");
            int result = 0;
            taskWorker.StartWork(1000);
            while (true)
            {
                result = GetMenuItem(menu);
                if (result > 0 && result < 6)
                {
                    CRUDElement(result);
                    continue;
                }   
                switch (result)
                {
                    case 6:
                        ShowLINQMenu();
                        break;
                    case 7:
                        ShowMessages();
                        break;
                    case 0:
                        return;
                    default:
                        TypeInfo("Enter correct menu option");
                        break;
                }
            }
        }
        private void CRUDElement(int elementId)
        {
            int result = 0;
            string element = dataTypes[elementId - 1];
            while (true)
            {
                result = GetMenuItem(CRUD.Select(x => x + " " + element).ToArray());
                switch (result)
                {
                    case 1:
                        AddAll(elementId);
                        break;
                    case 2:
                        ShowAll(elementId);
                        break;
                    case 3:
                        UpdateAll(elementId);
                        break;
                    case 4:
                        Delete(elementId, GetId());
                        break;
                    case 0:
                        return;
                    default:
                        TypeInfo("Enter correct menu option");
                        break;
                }
            }
        }
        
        private void ShowLINQMenu()
        {
            int result = 0;
            while (true)
            {
                result = GetMenuItem(lINQmenu);
                switch (result)
                {
                    case 1:
                        ShowProjectTasksCountByUserId();
                        break;
                    case 2:
                        ShowShortnameTasksByPerformerIdByUserId();
                        break;
                    case 3:
                        ShowFinishedTasksByUserId();
                        break;
                    case 4:
                        ShowSortedTeams();
                        break;
                    case 5:
                        ShowSortedUsersWithTasks();
                        break;
                    case 6:
                        ShowUserInfoById();
                        break;
                    case 7:
                        ShowProjectInfoById();
                        break;
                    case 0:
                        return;
                    default:
                        TypeInfo("Enter correct menu option");
                        break;
                }
            }
        }

        private int GetId()
        {
            TypeInfo("Enter Id");
            int res = -1;
            while (res == -1)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out res) || res <= 0)
                {
                    TypeInfo("Enter correct id, please");
                    res = -1;
                    continue;
                }
            }
            return res;
        }

        private int GetMenuItem(string[] menu)
        {
            for (int i = 1; i <= menu.Length; i++)
            {
                TypeInfo(i + " - " + menu[i - 1]);
            }
            Console.WriteLine();
            string result = "";
            int res = -1;
            while (res < 0)
            {
                result = Console.ReadLine();
                if (!int.TryParse(result, out res) || (res >= menu.Length || res < 0))
                {
                    res = -1;
                    TypeInfo("Please enter correct number");
                }
            }
            return res;
        }

        private void TypeInfo(string info)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(info);
            Console.ResetColor();
        }

        private async void Delete(int elementId, int Id)
        {
            await projectsService.Remove(dataTypes[elementId - 1], Id);
        }

        private async void ShowMessages()
        {
            var result = await projectsService.GetMessages();
            foreach(var str in result)
            {
                Console.WriteLine(str);
            }
        }


        #region ShowData
        private void ShowAll(int elementId)
        {
            Console.WriteLine($"All {dataTypes[elementId - 1]}s");
            switch (elementId)
            {
                case 1:
                    taskWorker.AddTaskToList(() => ShowList(projectsService.GetProjects()));
                    break;
                case 2:
                    taskWorker.AddTaskToList(() => ShowList(projectsService.GetStates()));
                    break;
                case 3:
                    taskWorker.AddTaskToList(() => ShowList(projectsService.GetTasks()));
                    break;
                case 4:
                    taskWorker.AddTaskToList(() => ShowList(projectsService.GetTeams()));
                    break;
                case 5:
                    taskWorker.AddTaskToList(() => ShowList(projectsService.GetUsers()));
                    break;
            }
            //var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);
        }
        public async Task<string> ShowList(dynamic list)
        {
            var rlist = await list;
            var result = "";
            foreach(var item in await list)
            {
                result += $"\t{item.ToString()}\n";
            }
            return result;
        }
        #endregion

        #region AddUpdateData
        public void UpdateAll(int elementId)
        {
            Console.WriteLine($"Updating {dataTypes[elementId - 1]}");
            switch (elementId)
            {
                case 1:
                    taskWorker.AddTaskToList(() => projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateProject(false)));
                    break;
                case 2:
                    taskWorker.AddTaskToList(() => projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateState(false)));
                    break;
                case 3:
                    taskWorker.AddTaskToList(() => projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateTask(false)));
                    break;
                case 4:
                    taskWorker.AddTaskToList(() => projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateTeam(false)));
                    break;
                case 5:
                    taskWorker.AddTaskToList(() => projectsService.UpdateData(dataTypes[elementId - 1], AddUpdateUser(false)));
                    break;
                case 0:
                    return;
            }
        }
        public void AddAll(int elementId)
        {
            Console.WriteLine($"Adding {dataTypes[elementId - 1]}");
            switch (elementId)
            {
                case 1:
                    taskWorker.AddTaskToList(() => projectsService.AddData(dataTypes[elementId -1], AddUpdateProject()));
                    break;
                case 2:
                    taskWorker.AddTaskToList(() => projectsService.AddData(dataTypes[elementId - 1], AddUpdateState()));
                    break;
                case 3:
                    taskWorker.AddTaskToList(() => projectsService.AddData(dataTypes[elementId - 1], AddUpdateTask()));
                    break;
                case 4:
                    taskWorker.AddTaskToList(() => projectsService.AddData(dataTypes[elementId - 1], AddUpdateTeam()));
                    break;
                case 5:
                    taskWorker.AddTaskToList(() => projectsService.AddData(dataTypes[elementId - 1], AddUpdateUser()));
                    break;
                case 0:
                    return;
            }
        }
        public Project AddUpdateProject(bool add = true)
        {
            var p = new Project();
            if(!add)
                p.Id = GetInt("current project id");
            p.Name = GetString("project name");
            p.Description = GetString("project description");
            if (add)
            {
                p.AuthorId = GetInt("author Id");
                p.Deadline = GetDate("deadline date");
                p.TeamId = GetInt("team id");
            }
            return p;
                
        }
        public State AddUpdateState(bool add = true)
        {
            var s = new State();
            if (!add)
                s.Id = GetInt("current state id");
            s.Value = GetString("state value");
            return s;
        }
        public Task AddUpdateTask(bool add = true)
        {
            var t = new Task();
            if (!add)
                t.Id = GetInt("current task id");
            t.Name = GetString("task name");
            t.Description = GetString("task description");
            t.StateId = GetInt("state id");
            if (!add)
                t.FinishedAt = GetDate("finished date");
            if (add)
            {
                t.PerformerId = GetInt("performer Id");
                t.ProjectId = GetInt("project id");
            }
            return t;
        }
        public Team AddUpdateTeam(bool add = true)
        {
            var t = new Team();
            if (!add)
                t.Id = GetInt("current team id");
            t.Name = GetString("Team name");
            return t;
        }
        public User AddUpdateUser(bool add = true)
        {
            var u = new User();
            if (!add)
                u.Id = GetInt("current user id");
            u.TeamId = GetInt("team id", true);
            u.Email = GetString("email");
            if (add)
            {
                u.FirstName = GetString("first name");
                u.LastName = GetString("last name");
                u.Birthday = GetDate("birthday");
            }
            return u;

        }
        #endregion

        public int GetInt(string name, bool canBeNull = false)
        {
            string canNull = canBeNull ? "or 0 for null" : "";
            TypeInfo($"Enter {name} {canNull}");
            int res = -1;
            while (res == -1)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out res) || res <= 0)
                {
                    TypeInfo($"Enter correct {name}, please");
                    res = -1;
                    continue;
                }
                else if (canBeNull && res == 0)
                    return 0;
            }
            return res;
        }
        public string GetString(string name)
        {
            TypeInfo($"Enter {name}");

            string result = Console.ReadLine();
            return result;
        }
        public DateTime GetDate(string name)
        {
            TypeInfo($"Enter {name} in format \"{DateTime.Now.Date}\"");
            while (true)
            {
                string result = Console.ReadLine();
                if (!DateTime.TryParse(result, out DateTime res) || res.Year <= 1950)
                {
                    TypeInfo($"Enter correct {name}, please");
                    continue;
                }
                else
                    return res;
            }
        }

        #region ProjectTasksCountByUserId
        private async void ShowProjectTasksCountByUserId()
        {
            TypeInfo("Enter UserId");
            var res = await projectsService.GetProjectTasksCountByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result");
            foreach (var pair in res)
            {
                TypeInfo("Project:");
                Console.WriteLine("\t" + pair.Project);
                Console.WriteLine("Tasks count: " + pair.TaskCount);
            }
        }
        #endregion

        #region ShortnameTasksByPerformerIdByUserId
        private async void ShowShortnameTasksByPerformerIdByUserId()
        {
            TypeInfo("Enter UserId");
            var res = await projectsService.GetShortnameTasksByPerformerIdByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var el in res)
            {
                Console.WriteLine("\t" + el);
            }
        }
        #endregion

        #region FinishedTasksByUserId
        private async void ShowFinishedTasksByUserId()
        {
            TypeInfo("Enter UserId");
            var res = await projectsService.GetFinishedTasksByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (Id, Name) in res)
            {
                Console.WriteLine("\tProject" + Id + " - " + Name);
            }
        }
        #endregion

        #region SortedTeams
        private async void ShowSortedTeams()
        {
            var res = await projectsService.GetSortedTeams();
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (id, name, userList) in res)
            {
                Console.WriteLine("\tProject:" + id + " - " + name);
                TypeInfo("\tUsers:");
                foreach (var u in userList)
                    Console.WriteLine("\t\t" + u.ToString());
            }
        }
        #endregion

        #region SortedUsersWithTasks
        private async void ShowSortedUsersWithTasks()
        {
            var res = await projectsService.GetSortedUsersWithTasks();
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (user, tasks) in res)
            {
                TypeInfo("\tUser:");
                Console.WriteLine("\t\t" + user);
                TypeInfo("\tTasks:");
                foreach (var t in tasks)
                    Console.WriteLine("\t\t\t" + t.ToString());
            }
        }
        #endregion

        #region UserInfoById
        private async void ShowUserInfoById()
        {
            TypeInfo("Enter UserId");
            var res = await projectsService.GetUserInfoById(GetId());
            if (res.User == null)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            Console.WriteLine("\tUser: " + res.User);
            Console.WriteLine("\tLastProject: " + res.LastProject);
            Console.WriteLine("\tCanceledTasksCount: " + res.CanceledTasksCount);
            Console.WriteLine("\tLongestTask: " + res.LongestTask);
            Console.WriteLine("\tLastProjectUserTasks: " + res.LastProjectUserTasks);
        }
        #endregion

        #region ProjectInfoById
        private async void ShowProjectInfoById()
        {
            TypeInfo("Enter ProjectId");
            var res = await projectsService.GetProjectInfoById(GetId());
            if (res.Project == null)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            Console.WriteLine("\tProject: " + res.Project.ToString());
            Console.WriteLine("\tLongestTask: " + res.LongestTask.ToString());
            Console.WriteLine("\tShortestTask: " + res.ShortestTask.ToString());
            if (res.UsersCount != -1)
                Console.WriteLine("\tUsersCount: " + res.UsersCount);
        }
        #endregion
    }
}
