﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class HubService
    {
        HubConnection _connection;
        int faultAttemptCount = 0;
        public HubService(string url)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(url)
                .Build();
        }

        public HubConnection Connection => _connection;

        public async void StartConnection()
        {
            _connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await _connection.StartAsync().ContinueWith(task => ConnectionResult(task));
            };

            _connection.On<string>("displayMessage", (message) =>
            {
                Console.WriteLine($"\tServer corresponds with: {message}");
            });

            await Start();
        }

        private async Task Start()
        {
            await _connection.StartAsync().ContinueWith(task => ConnectionResult(task));
            if (faultAttemptCount > 0)
            {
                await Task.Delay(faultAttemptCount * 1000);
                await Start();
            }
        }

        private void ConnectionResult(Task task)
        {
            if (task.IsFaulted)
            {
                Console.WriteLine("There was an error opening the connection");
                IncreaseTime();
            }
            else
            {
                Console.WriteLine("Connected to server");
                faultAttemptCount = 0;
            }
        }

        private void IncreaseTime()
        {
            faultAttemptCount += faultAttemptCount == 30 ? 0 : 1;
        }
    }
}
