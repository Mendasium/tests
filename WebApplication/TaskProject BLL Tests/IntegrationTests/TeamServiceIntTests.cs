﻿using AutoMapper;
using BLL.Services;
using BLL.Services.ServicesAbstraction;
using DataAccess;
using DataAccess.Repositories;
using Model.Model;
using Model.Model.DTOModel;
using Moq;
using System;
using System.Linq;
using TaskProject.StartupConfiguration;
using TaskProject_BLL_Tests.Fake;
using Xunit;

namespace TaskProject_BLL_Tests.IntegrationTests
{
    public class TeamServiceIntTests
    {
        [Fact]
        public async void Create_When_WeHaveCorrectDTOTeam_Then_CheckIfItWasAddedToTheContext()
        {
            var dTOTeam = new DTOTeam()
            {
                Name = "New Team"
            };
            
            using (TaskProjectDBContext dBContext = new FakeDbContext("Teams_Create").InitializeWithData().DbContext)
            {
                IMapper mapper = MapperConfig.ConfigureMapping();
                var teamRep = new Repository<Team>(dBContext);
                var queueMock = new Mock<IQueueService>();

                var teamService = new TeamService(teamRep, queueMock.Object, mapper);

                int beforeTeamsCount = (await teamService.GetEntities()).Count();

                var res = await teamService.Create(dTOTeam);

                int afterTeamCount = (await teamService.GetEntities()).Count();
                
                Assert.Equal(1, afterTeamCount - beforeTeamsCount);
            }
            
        }
    }
}
