﻿using AutoMapper;
using BLL.Services;
using BLL.Services.ServicesAbstraction;
using DataAccess;
using DataAccess.Repositories;
using Model.Model;
using Model.Model.DTOModel;
using Moq;
using System;
using System.Linq;
using TaskProject.StartupConfiguration;
using TaskProject_BLL_Tests.Fake;
using Xunit;

namespace TaskProject_BLL_Tests.IntegrationTests
{
    public class ProjectServiceIntTests
    {
        [Fact]
        public async void Create_When_WeHaveCorrectDTOProject_Then_CheckIfItWasAddedToTheContext()
        {
            var dTOProject = new DTOProject()
            {
                Name = "New Project",
                TeamId = 12,
                Description = "A great amount of text",
                Deadline = DateTime.Now.AddDays(21),
                AuthorId = 12
            };



            var context = new FakeDbContext("Projects_Add");
            context.InitializeWithData();
            using (TaskProjectDBContext dBContext = context.DbContext)
            {
                IMapper mapper = MapperConfig.ConfigureMapping();
                var taskRep = new Repository<Task>(dBContext);
                var projectRep = new Repository<Project>(dBContext);
                var userRep = new Repository<User>(dBContext);
                var stateRep = new Repository<State>(dBContext);
                var teamRep = new Repository<Team>(dBContext);
                var queueMock = new Mock<IQueueService>();

                var projectService = new ProjectService(projectRep, userRep, teamRep, taskRep, stateRep, queueMock.Object, mapper);

                int beforeTeamsCount = (await projectService.GetEntities()).Count();

                await projectService.Create(dTOProject);

                int afterTeamCount = (await projectService.GetEntities()).Count();
                
                Assert.Equal(1, afterTeamCount - beforeTeamsCount);
            }
            
        }
    }
}
