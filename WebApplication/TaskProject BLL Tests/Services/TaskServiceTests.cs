﻿using AutoMapper;
using BLL.Services;
using BLL.Services.ServicesAbstraction;
using DataAccess.Repositories;
using Model.Model;
using Model.Model.DTOModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskProject.StartupConfiguration;
using TaskProject_BLL_Tests.Fake;
using Xunit;
using Task = Model.Model.Task;

namespace TaskProject_BLL_Tests.Services
{
    public class TaskServiceTests
    {
        [Fact]
        public async void Update_When_NeedToChangeStateStateToDoneUsingId_Then_CheckTaskState()
        {
            var createdTasks = new List<Task>()
            {
                new Task()
                {
                    Id = 2,
                    StateId = 3
                }
            };
            var task = new DTOTask()
            {
                Id = 2,
                StateId = 1,
                Description = "new description for task",
                FinishedAt = DateTime.Now
            };


            IMapper mapper = MapperConfig.ConfigureMapping();
            var fakeTaskRep = new FakeTaskRepository(createdTasks);

            var userMock = new Mock<IRepository<User>>();
            //userMock.Setup(rep => rep.GetAsync(It.IsAny<Func<User, bool>>()))
            //    .Returns(ReturnExpectedTeam());
            var projectMock = new Mock<IRepository<Project>>();
            //projectMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Project, bool>>()))
            //    .Returns(ReturnExpectedTeam());
            var statesMock = new Mock<IRepository<State>>();
            statesMock.Setup(rep => rep.GetAsync(It.IsAny<Func<State, bool>>()))
                .Returns(ReturnStates());
            var queueMock = new Mock<IQueueService>();
            queueMock.Setup(service => service.Send(""));

            var taskService = new TaskService(fakeTaskRep, userMock.Object, projectMock.Object, statesMock.Object, queueMock.Object, mapper);

            await taskService.Update(task);

            var result = await fakeTaskRep.GetAsync();
            Assert.Equal("Done", result.FirstOrDefault().State.Value);
        }

        private async Task<IEnumerable<State>> ReturnStates()
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() => new List<State>()
            {
                new State() { Id = 1, Value = "Done" },
                new State() { Id = 2, Value = "Created" },
                new State() { Id = 3, Value = "In Progress" },
                new State() { Id = 4, Value = "finished" }
            });
        }

        [Fact]
        public async void GetShortnameTasksByUserId_When_UserIdIsThree_Then_ThereAreTwoCorrectResults()
        {
            var tasks = new List<Task>()
            {
                new Task(){ Id = 1, Name = "Task1", PerformerId = 2 },
                new Task(){ Id = 2, Name = "Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2Task2" +
                "Task2Task2Task2Task2Task2Task2", PerformerId = 3 },
                new Task(){ Id = 3, Name = "Task3", PerformerId = 1 },
                new Task(){ Id = 4, Name = "Task4", PerformerId = 3 },
                new Task(){ Id = 5, Name = "Task5", PerformerId = 3 }
            };
            var users = new List<User>()
            {
                new User(){ Id = 1, TeamId = 2, Birthday = DateTime.Now.AddYears(-15) },
                new User(){ Id = 2, TeamId = 2, Birthday = DateTime.Now.AddYears(-14) },
                new User(){ Id = 3, TeamId = 3, Birthday = DateTime.Now.AddYears(-15) }
            };

            IMapper mapper = MapperConfig.ConfigureMapping();


            var taskMock = new Mock<IRepository<Task>>();
            taskMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Task, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => tasks.Select(x => x)));

            var userMock = new Mock<IRepository<User>>();
            userMock.Setup(rep => rep.GetAsync(It.IsAny<Func<User, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => users.Select(x => x)));

            var projectMock = new Mock<IRepository<Project>>();
            var statesMock = new Mock<IRepository<State>>();
            var queueMock = new Mock<IQueueService>();
            queueMock.Setup(service => service.Send(""));

            var taskService = new TaskService(taskMock.Object, userMock.Object, projectMock.Object, statesMock.Object, queueMock.Object, mapper);

            var result = await taskService.GetShortnameTasksByUserId(3);

            Assert.Equal(2, result.Count());
            Assert.Equal(4, result.ToList()[0].Id);
            Assert.Equal(5, result.ToList()[1].Id);
        }

        [Fact]
        public async void GetFinishedTasksByUserId_When_UserIdIsThreeAndYearIs2019_Then_ThereIsOneCorrectResults()
        {
            var tasks = new List<Task>()
            {
                new Task(){ Id = 1, Name = "Task1", PerformerId = 2 },
                new Task(){ Id = 2, Name = "Task2", PerformerId = 3, FinishedAt = DateTime.Now, StateId = 4  },
                new Task(){ Id = 3, Name = "Task3", PerformerId = 1 },
                new Task(){ Id = 4, Name = "Task4", PerformerId = 3, FinishedAt = DateTime.Now.AddYears(-2), StateId = 4 },
                new Task(){ Id = 5, Name = "Task5", PerformerId = 3, FinishedAt = DateTime.Now, StateId = 3 }
            };

            IMapper mapper = MapperConfig.ConfigureMapping();


            var taskMock = new Mock<IRepository<Task>>();
            taskMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Task, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => tasks.Select(x => x)));

            var userMock = new Mock<IRepository<User>>();

            var projectMock = new Mock<IRepository<Project>>();
            var statesMock = new Mock<IRepository<State>>();
            statesMock.Setup(rep => rep.GetAsync(It.IsAny<Func<State, bool>>()))
                .Returns(ReturnStates());
            var queueMock = new Mock<IQueueService>();
            queueMock.Setup(service => service.Send(""));

            var taskService = new TaskService(taskMock.Object, userMock.Object, projectMock.Object, statesMock.Object, queueMock.Object, mapper);

            var result = await taskService.GetFinishedTasksByUserId(3, 2019);

            Assert.Single(result);
            Assert.Equal(2, result.ToList()[0].Id);
        }

        [Fact]
        public async void GetSortedUsersWithTasks_When_WeHaveFourUsersThreeOfWhichHaveTasks_Then_GetCorrectStructure()
        {
            var tasks = new List<Task>()
            {
                new Task(){ Id = 1, Name = "Task1", PerformerId = 2 },
                new Task(){ Id = 2, Name = "Task211", PerformerId = 3 },
                new Task(){ Id = 3, Name = "Task3", PerformerId = 1 },
                new Task(){ Id = 4, Name = "Task41", PerformerId = 3 },
                new Task(){ Id = 5, Name = "Task5111", PerformerId = 3 }
            };
            var users = new List<User>()
            {
                new User(){ Id = 1, TeamId = 2, FirstName = "Andrii" },
                new User(){ Id = 2, TeamId = 2, FirstName = "Yurii" },
                new User(){ Id = 3, TeamId = 3, FirstName = "Oleksii" },
                new User(){ Id = 4, TeamId = 3, FirstName = "Ivan" }
            };

            IMapper mapper = MapperConfig.ConfigureMapping();


            var taskMock = new Mock<IRepository<Task>>();
            taskMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Task, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => tasks.Select(x => x)));

            var userMock = new Mock<IRepository<User>>();
            userMock.Setup(rep => rep.GetAsync(It.IsAny<Func<User, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => users.Select(x => x)));

            var projectMock = new Mock<IRepository<Project>>();
            var statesMock = new Mock<IRepository<State>>();
            var queueMock = new Mock<IQueueService>();
            queueMock.Setup(service => service.Send(""));

            var taskService = new TaskService(taskMock.Object, userMock.Object, projectMock.Object, statesMock.Object, queueMock.Object, mapper);

            var result = await taskService.GetSortedUsersWithTasks();

            Assert.Equal(3, result.Count());
            Assert.Equal(1, result.ToList()[0].user.Id);
            Assert.Equal(3, result.ToList()[1].user.Id);
            Assert.Equal(2, result.ToList()[2].user.Id);
            Assert.Equal(3, result.ToList()[1].tasks.Count());
            Assert.Equal(5, result.ToList()[1].tasks.ToList()[0].Id);
            Assert.Single(result.ToList()[2].tasks);
        }
    }
}
