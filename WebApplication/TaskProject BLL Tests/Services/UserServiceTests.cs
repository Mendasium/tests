﻿using AutoMapper;
using BLL.Services;
using BLL.Services.ServicesAbstraction;
using DataAccess.Repositories;
using Model.Model;
using Model.Model.DTOModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TaskProject.StartupConfiguration;
using TaskProject_BLL_Tests.Fake;
using Xunit;

namespace TaskProject_BLL_Tests.Services
{   
    public class UserServiceTests
    {
        public UserServiceTests()
        {

        }

        [Fact]
        public async void Create_When_UserWithoutTeam_Then_AddUserToReposWihoutMistakes()
        {
            var user = new DTOUser()
            {
                FirstName = "Ivan",
                LastName = "Ivan",
                TeamId = 2,
                Email = "214iuijg",
                Birthday = DateTime.Now
            };

            IMapper mapper = MapperConfig.ConfigureMapping();
            var userMock = new Mock<IRepository<User>>();
            userMock.Setup(userR => userR.SaveAsync(It.IsAny<CancellationToken>()));
            userMock.Setup(userR => userR.Create(It.IsAny<User>()));
            userMock.Setup(userR => userR.GetAsync(It.IsAny<Func<User, bool>>()))
                .Returns(ReturnUserCollectionWithZeroId());
            var teamMock = new Mock<IRepository<Team>>();
            teamMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Team, bool>>()))
                .Returns(ReturnEmptyCOllectionOfTeams());
            var queueMock = new Mock<IQueueService>();
            queueMock.Setup(service => service.Send(""));
            
            UserService userService = new UserService(userMock.Object, teamMock.Object, queueMock.Object, mapper);

            var result = await userService.Create(user);

            userMock.Verify(r => r.Create(It.IsAny<User>()));
            userMock.Verify(r => r.SaveAsync(It.IsAny<CancellationToken>()));
            Assert.NotNull(result);
        }

        private async Task<IEnumerable<Team>> ReturnEmptyCOllectionOfTeams()
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() => new List<Team>());
        }

        private async Task<IEnumerable<User>> ReturnUserCollectionWithZeroId()
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() => new List<User>() { new User() { Id = 0 } });
        }

        [Fact]
        public async void AddUserToTeam_When_TeamExists_Then_GetUserWithCorrectTeamId()
        {
            var createdUsers = new List<User>()
            {
                new User()
                {
                    Id = 2,
                    TeamId = 0,
                    Birthday = DateTime.Now
                }
            };
            var user = new DTOUser()
            {
                Id = 2,
                FirstName = "Ivan",
                LastName = "Ivan",
                TeamId = 2,
                Email = "214iuijg",
                Birthday = DateTime.Now
            };


            IMapper mapper = MapperConfig.ConfigureMapping();
            var fakeUserRep = new FakeUserRepository(createdUsers);
            var teamMock = new Mock<IRepository<Team>>();
            teamMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Team, bool>>()))
                .Returns(ReturnExpectedTeam());
            var queueMock = new Mock<IQueueService>();
            queueMock.Setup(service => service.Send(""));

            var userService = new UserService(fakeUserRep, teamMock.Object, queueMock.Object, mapper);

            await userService.Update(user);

            var result = await fakeUserRep.GetAsync();
            var teams = await ReturnExpectedTeam();
            Assert.Equal(result.FirstOrDefault().Team.Id, teams.FirstOrDefault().Id); 
        }

        private async Task<IEnumerable<Team>> ReturnExpectedTeam()
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() => new List<Team>()
            {
                new Team()
                {
                    Id = 2, Name = "Second Team"
                }
            });
        }

        [Fact]
        public async void GetSortedTeams_When_SuchResultExists_Than_GetNotNullList()
        {
            var teams = new List<Team>()
            {
                new Team(){ Id = 1, Name = "Team1" },
                new Team(){ Id = 2, Name = "Team2" },
                new Team(){ Id = 3, Name = "Team3" },
            };
            var users = new List<User>()
            {
                new User(){ Id = 1, TeamId = 2, Birthday = DateTime.Now.AddYears(-15) },
                new User(){ Id = 2, TeamId = 2, Birthday = DateTime.Now.AddYears(-14) },
                new User(){ Id = 3, TeamId = 3, Birthday = DateTime.Now.AddYears(-15) },
                new User(){ Id = 4, TeamId = 3, Birthday = DateTime.Now.AddYears(-11) },
                new User(){ Id = 4, TeamId = 1, Birthday = DateTime.Now.AddYears(-11) },
            };

            IMapper mapper = MapperConfig.ConfigureMapping();
            var queueMock = new Mock<IQueueService>();
            queueMock.Setup(service => service.Send(""));

            var teamMock = new Mock<IRepository<Team>>();
            teamMock.Setup(rep => rep.GetAsync(It.IsAny<Func<Team, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => teams.Select(x => x)));

            var userMock = new Mock<IRepository<User>>();
            userMock.Setup(rep => rep.GetAsync(It.IsAny<Func<User, bool>>()))
                .Returns(() => System.Threading.Tasks.Task.Factory.StartNew(() => users.Select(x => x)));

            var userService = new UserService(userMock.Object, teamMock.Object, queueMock.Object, mapper);

            var result = await userService.GetSortedTeams();

            Assert.Single(result);
            Assert.Equal(2, result.First().id);
            Assert.Equal(2, result.First().userList.Count());
        }
    }
}
