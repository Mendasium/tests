﻿using DataAccess.Repositories;
using Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskProject_BLL_Tests.Fake
{
    class FakeUserRepository : IRepository<User>
    {
        List<User> _users;

        public FakeUserRepository(IEnumerable<User> users = null)
        {
            _users = new List<User>();
            if (users?.Count() > 0)
                _users.AddRange(users);
        }

        public void Create(User entity)
        {
            if (entity == null)
                throw new NullReferenceException();
            _users.Add(entity);
        }

        public void Delete(User entity)
        {
            var user = _users.FirstOrDefault(x => x.Id == entity.Id);
            if (user == null)
                throw new NullReferenceException();
            _users.Remove(user);
        }

        public void Delete(int id)
        {
        }

        public Task<IEnumerable<User>> GetAsync(Func<User, bool> filter = null)
        {
            if (filter != null)
                return System.Threading.Tasks.Task.Factory.StartNew(() => _users.Where(filter));
            else
                return System.Threading.Tasks.Task.Factory.StartNew(() => _users.Select(u => u));
        }

        public System.Threading.Tasks.Task SaveAsync(CancellationToken token)
        {
            return System.Threading.Tasks.Task.Delay(1);
        }
        
        public void Smth() { }

        public void Update(User entity)
        {
            var e = _users.FirstOrDefault(x => x.Id == entity.Id);
            if (e != null)
            {
                e.Update(entity);
            }
        }
    }
}
