﻿using DataAccess.Repositories;
using Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TaskProject_BLL_Tests.Fake
{
    class FakeTaskRepository : IRepository<Task>
    {
        List<Task> _tasks;

        public FakeTaskRepository(IEnumerable<Task> tasks = null)
        {
            _tasks = new List<Task>();
            if (tasks?.Count() > 0)
                _tasks.AddRange(tasks);
        }

        public void Create(Task entity)
        {
            if (entity == null)
                throw new NullReferenceException();
            _tasks.Add(entity);
        }

        public void Delete(Task entity)
        {
            var user = _tasks.FirstOrDefault(x => x.Id == entity.Id);
            if (user == null)
                throw new NullReferenceException();
            _tasks.Remove(user);
        }

        public void Delete(int id)
        {
        }

        public System.Threading.Tasks.Task<IEnumerable<Task>> GetAsync(Func<Task, bool> filter = null)
        {
            if (filter != null)
                return System.Threading.Tasks.Task.Factory.StartNew(() => _tasks.Where(filter));
            else
                return System.Threading.Tasks.Task.Factory.StartNew(() => _tasks.Select(u => u));
        }

        public System.Threading.Tasks.Task SaveAsync(CancellationToken token)
        {
            return System.Threading.Tasks.Task.Delay(1);
        }
        
        public void Smth() { }

        public void Update(Task entity)
        {
            var e = _tasks.FirstOrDefault(x => x.Id == entity.Id);
            if (e != null)
            {
                e.Update(entity);
            }
        }
    }
}
