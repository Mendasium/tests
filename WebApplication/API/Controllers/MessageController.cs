﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BLL.Services;
using AutoMapper;
using System.Threading.Tasks;
using BLL.Services.ServicesAbstraction;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        IMessageService _messageService;

        public MessagesController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet]
        public async Task<IEnumerable<string>> GetAll()
        {
            return await _messageService.ReadAll();
        }
    }
}
