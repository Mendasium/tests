﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services.ServicesAbstraction
{
    public interface IMessageService
    {
        Task<IEnumerable<string>> ReadAll(); 
    }
}
