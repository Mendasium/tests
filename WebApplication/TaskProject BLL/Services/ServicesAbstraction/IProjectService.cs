﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Model.Model.DTOModel;

namespace BLL.Services.ServicesAbstraction
{
    public interface IProjectService : IService<DTOProject>
    {
        Task<IEnumerable<(DTOProject Project, int TaskCount)>> GetProjectTasksCountByUserIdAsync(int id);
        Task<(DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks)> GetUserInfoById(int id);
        Task<(DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount)> GetProjectInfoById(int id);
    }
}
