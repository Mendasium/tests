﻿
namespace BLL.Services.ServicesAbstraction
{
    public interface IQueueService
    {
        void Send(string message);
    }
}
