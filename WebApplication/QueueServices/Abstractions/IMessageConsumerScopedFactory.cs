﻿using QueueService.Model;
using QueueServices.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Abstractions
{
    public interface IMessageConsumerScopedFactory
    {
        IMessageConsumerScoped Open(MessageScopedSettings messageScopedSettings);
        IMessageConsumerScoped Connect(MessageScopedSettings messageScopedSettings);
    }
}
