﻿using QueueService.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueueServices.Abstractions
{
    public interface IMessageProducerScopedFactory
    {
        IMessageProducerScoped Open(MessageScopedSettings setting);
    }
}
